+++
author = "Rudy GUYENNOT"
date = "2021-12-21"
title = "☸ KUBERNETES - On Premise 🏠 - REX sur un context client"
description = "Guide to emoji usage in Hugo"
tags = [
"kubernetes",
"devops",
"gitops"
]
toc = true
+++

## Introduction
**📝 RETOUR D'EXPERIENCE SUR UN CONTEXTE CLIENT 📝**

Début 2020, un projet *DevOps* a été initié chez un de nos clients pour répondre à plusieurs besoins.
Nous sommes dans un contexte où l'équipe d'exploitation (*Ops*) gère un grand nombre d'applications métier très hétérogènes avec un scope de responsabilités assez vaste, 
allant des *facilities* (gestion des salles serveurs), du provisionnement de serveurs virtuels (VMs,) jusqu'au déploiement des applications en production, 
en intégrant évidemment le monitoring applicatif, le stockage SAN, le backup disques/LTO7.

Les processus en place n'étaient plus suffisament efficients et les demandes d'hébergement, de déploiement en production grandissant, il fallait repenser certaines méthodes de travail. 
Cette perte d'efficacité était en quelque sorte liée au *turn-over* régulier dans l'équipe d'*Ops*, au large *scope* d'intervention mais aussi et surtout, au manque de dialogue avec les (*nombreuses*) équipes de développeurs.

La première phase de ce projet a été de faire en sorte que l'equipe des *Ops* adopte la méthodologie *DevOps* (mais surtout *GitOps*) pour la faire gagner en agilité, afin de pouvoir ensuite migrer sur une architecture résiliente et automatisée.

La phase suivante a été de reprendre les processus d'exploitation de l'infrastructure *legacy* et de trouver la meilleure solution qui s'offrait pour répondre à tous les besoins du client.

A partir de là, le choix s'est rapidement porté sur la solution ☸ [Kubernetes](https://kubernetes.io/fr/) ☸

Dans cet article, nous allons expliquer ce choix et présenter les problématiques rencontrées sur sa mise en place dans l'infrastructrure *on premise* du client.

## Choix de la solution Kubernetes ☸

*A quels besoins Kubernetes permet-il de répondre et à quelles contraintes s'expose-t-on dans un environnement *on premise* ?*

**Kubernetes ☸** est avant tout une solution open source initiée par **Google** pour orchestrer énormément de conteneurs en interne, puis le projet a été ouvert et est vite devenu une référence du catalogue de la [CNCF](https://landscape.cncf.io/card-mode?sort=stars) (*Cloud Native Computing Fundation*). Ce qui fait la force de cet outil, c'est la possibilité de créer un cluster sur des hôtes qui soient *on premise* (datacenters internes à l'entreprise) ou dans tous les types de *cloud-computing*.  
**Kubernetes ☸** est maintenant bien connu et cette notoriété se distingue facilement lorqu'on se penche sur les différents services managés, proposés par les principaux fournisseurs de Cloud du marché actuel (*EKS chez Amazon*, *AKS chez Azure*, *GKE chez Google*). De plus en plus d'entreprises migrent vers ce type d'infrastructure et les profils sur le marché de l'IT sont de plus en plus recherchés pour assurer le *Build & Run* autour de cette solution.

La communauté *GitOps* autour de Kubernetes ☸ est très active, tout comme la fréquence des sorties de versions  :
{{% notice tip "Tip" %}}
- Une majeure tous les 3 mois
- Une mineure tous les mois
{{% /notice %}}
{{% notice warning "Warning" %}}
Seules les 3 dernières versions sont maintenues (corrections de CVE)
{{% /notice %}}
En introduction, nous soulignions le fait que les processus en place dans l'équipe d'Ops nétaient plus assez efficients en comparaison du nombre de demandes et qu'il manquait une certaine *"agilité"*.

Les mécanismes internes de **Kubernetes**, s’ils sont bien utilisés et en respectant la démarche *DevOps*, doivent normalement permettre de palier à plusieurs de ces difficultés...à condition d'avoir une équipe d'Ops qui soit suffisament bien dimensionnée mais surtout bien **qualifiée** !

C'est pourquoi dans notre contexte client, pour accélérer la montée en compétence de l'équipe, un *mentor DevOps* avec une expertise avancée de `Kubernetes` | `Helm` | `IaC` (*Infrastructure As Code*) a été détaché directement dans l'équipe d'Ops.

**Le client avait plusieurs besoins initiaux** :

- Conserver les données en interne
- Maîtrise des coûts liés à l'infrastructure
- Améliorer la fréquence et la qualité des déploiements des applications métier via pipelines CI/CD
- Résilience accrue des applications (qu'on peut obtenir grâce à Kubernetes avec le *self-healing, liveness/readyness...*)
- Rendre les équipes de développeurs plus autonomes
- *Conteneuriser* ce qui peut l’être et repenser les applications en *micro-service*
- Faciliter la gestion de l’infrastructure et des OS liés au contexte client
- Décrire l’infrastructure *As Code*
- Mutualiser les bonnes pratiques entre les *Dev* et les *Ops*

Avoir la gestion complète d'un cluster **Kubernetes ☸** sur un environnement *on-premise* est bien différent d'un cluster *Elastic Kubernetes Service (EKS)* chez **Amazon AWS** par exemple. Avec AWS, le déploiement d'un cluster est simplifié au maximum et vous n'avez pas à vous préoccuper du *control-plane* contenant les *masters-nodes*. Tout est géré par les équipes IT d'**Amazon AWS**.

Sur une infrastructure **on-premise**, il faut gérer l’installation et le déploiement complet du cluster, d'où l'importance d'avoir des équipes d'Ops qualifiées, avec une bonne connaissance des mécanismes et des composants internes de **Kubernetes ☸**.

Voici les principaux composants d’un cluster **Kubernetes ☸** :

| Composant | Rôle |
| --- | --- |
| **Etcd** | Base clé/valeur qui stocke les données d’état du cluster, elle est généralement déployée en mode cluster sur les nœuds du *control-plane*. |
| **Kube-scheduler** | Composant dont le rôle est d’assigner des pods à des nœuds du data plane en se basant sur des données contenues dans l’Etcd. |
| **Kube-controller-manager** | Daemon qui observe en continu l’état du cluster, c’est un composant au cœur du fameux mécanisme de boucle de régulation. |
| **Kube-api-server** | Serveur exposant une API REST, valide et applique les changements sur des ressources du cluster. |

Pour être capable de déployer le plus facilement et le plus rapidement possible, un cluster **Kubernetes ☸** dans l'infrastructure du client, il n'y avait pas d'autre choix que d'automatiser au maximum toute la chaîne.

C'est là qu'entre en jeu un terme bien à la mode, celui d'**Infrastrcuture As Code** (*IaC*).


## Infrastructure As Code ✍️

Cette phase du projet a été cruciale car il fallait absolument être capable de déployer (ou de re-déployer) un cluster **Kubernetes ☸** en quelques minutes,  
chose inenvisageable à la main car il y a trop de sources d'erreur, manque de reproductibilité...

L'utilisation de plus en plus massive de **Kubernetes ☸** sur des infrastructures critiques de grandes entreprises, a permis l'émergence d'outils *cloud-native* pour justement répondre à ces problématiques de plus en plus fréquentes chez les équipes d'Ops.

La société [HashiCorp](https://www.hashicorp.com/) en a fait son businness en proposant notamment deux outils **libres et open source**, capables de déployer de l’infrastructure à partir de fichiers descriptifs :
- [Packer](https://www.packer.io/)
- [Terraform](https://www.terraform.io/)

Un troisième outil **libre et open source**  a été utilisé pour faire de la gestion de configuration :
- [Ansible](https://www.ansible.com/)


**A quoi servent-ils respectivement ?**

| **Packer** | **Terraform** | **Ansible** |
| --- | --- | --- |
| Il est utilisé pour la création d'images immuables multi-plateformes (templates de VMs). | Il permet aux Ops de prédire de manière certaine, la création, le changement et l'évolution de l'infrastructure via du code versionné. | Les playbooks et les rôles Ansible servent à provisionner les fichiers de configuration ou les fichiers système.

Ce qui fait la force de ces outils, c'est l'utilisation des API REST pour dialoguer avec les infrastrastructures et sont donc compatibles avec pratiquement tous les **providers** du marché (*VMware, AWS, OpenShift…*).
> Le gros avantage de ces outils orientés *GitOps*, c'est justement l'utilisation de **Git** et le fait de bénéficier d'un système de *branching* et de *versionning*.  Tout le code qui décrit notre infrastructure **Kubernetes** est alors stocké dans le GitLab hébergé en interne.

Dans notre contexte client, l'infrastructure en place repose sur un cluster constitué de 10 hôtes ESXi **VMware vSphere 6.7**, la rendant compatible avec ces trois outils.


### Extrait de code Packer

Voici un extrait de code qui permet de *builder* un template de VM CentOS utilisé par **Terraform** pour déployer le cluster **Kubernetes** :

```yaml
{
    "variables": {
      "APP":                      "k8s",
      "PACKER_VCENTER_PASSWORD":  "{{env `PACKER_VCENTER_PASSWORD`}}",
      "REMOTE_SSH_USER":          "admsrv",
      "REMOTE_SSH_PASSWORD":      "{{env `REMOTE_SSH_PASSWORD`}}",
      "VERSION":                  "1.2.6"
    },
    "sensitive-variables": [
      "REMOTE_SSH_PASSWORD"
    ],  
    "builders": [
      {    
        "type":                   "vsphere-clone",
        "vcenter_server":         "10.xx.xx.xx",
        "username":               "packer@vsphere.local",
        "password":               "{{user `PACKER_VCENTER_PASSWORD`}}",
        "insecure_connection":    "true",
        "cluster":                "ZAT - Standard",
        "folder":                 "MODELES/PACKER",
        "vm_name":                "tmpl-centos-7.7-{{user `APP`}}-{{user `VERSION`}}",
        "CPUs":                   2,
        "RAM":                    2048,
        "RAM_reserve_all":        true,
        "convert_to_template":    "true",
        "datastore":              "datastore_name",
        "network":                "vlan-id",
        "boot_order":             "disk,cdrom",
        "template":               "tmpl-centos-7.7-base-1.0.2",
        "ssh_username":           "{{user `REMOTE_SSH_USER`}}",
        "ssh_password":           "{{user `REMOTE_SSH_PASSWORD`}}",
        "customize": {
          "linux_options": {
            "host_name":          "dtgsrv-{{user `APP`}}",
            "domain":             "dns_zone_name",
            "time_zone":           "Europe/Paris"     
          },
          "network_interface": {
            "ipv4_address":       "10.xx.xx.xx",
            "ipv4_netmask":       "25"
          },
          "ipv4_gateway":         "10.xx.xx.xx",
          "dns_server_list": [
            "10.22.98.1",
            "130.98.194.189",
            "192.196.111.47"
          ],
          "dns_suffix_list": [
            "dns_servers"
          ]
        },
        "communicator":           "ssh",
        "tools_upgrade_policy":   true,
        "tools_sync_time":        true
      }
    ],
    "provisioners": [
      {
        "type": "ansible-local",
        "playbook_file": "/data/ansible/base/main.yml",
        "extra_arguments": [
          "-e host_var=localhost",
          "-e username={{ user `REMOTE_SSH_USER`}}",
          "-e password={{ user `REMOTE_SSH_PASSWORD`}}",
          "-e ansible_become_pass={{ user `REMOTE_SSH_PASSWORD`}}"
        ],
        "role_paths": [
          "/data/ansible/base/roles/yum",
          "/data/ansible/base/roles/bashrc",
          "/data/ansible/base/roles/wgetrc",
          "/data/ansible/base/roles/ssl_certs",
          "/data/ansible/base/roles/fusioninventory",
          "/data/ansible/base/roles/proxy",
          "/data/ansible/base/roles/snmp",
          "/data/ansible/base/roles/ssh_keys"
        ]
      },
      {
        "type": "ansible-local",
        "playbook_file": "/data/ansible/docker/main.yml",
        "extra_arguments": [
          "-e host_var=localhost",
          "-e username={{ user `REMOTE_SSH_USER`}}",
          "-e password={{ user `REMOTE_SSH_PASSWORD`}}",
          "-e ansible_become_pass={{ user `REMOTE_SSH_PASSWORD`}}"
        ],
        "role_paths": [
          "/data/ansible/docker/roles/docker-ce"
        ]
      },
      {
        "type": "ansible-local",
        "playbook_file": "/data/ansible/kubernetes/main.yml",
        "extra_arguments": [
          "-e host_var=localhost",
          "-e username={{ user `REMOTE_SSH_USER`}}",
          "-e password={{ user `REMOTE_SSH_PASSWORD`}}",
          "-e ansible_become_pass={{ user `REMOTE_SSH_PASSWORD`}}"
        ],
        "role_paths": [
          "/data/ansible/kubernetes/roles/k8s"
        ]
      }
    ]
  }
```

### Extrait de code Terraform

Ci-dessous, un extrait de code Terraform qui va déployer et initialiser le cluster **Kubernetes** :

```tf
module "k8s_development" {
    source = "../../modules/k8s"

    shortEnv = "p"
    vsphere_network = "prod-vlan-id"
    
    masters = {
        master-1 = {
            vsphere_datastore   = "Datastore_name"
            vsphere_vm_template = "tmpl-centos-7.7-k8s-1.2.5"
            ip_address          = "10.xx.xx.xx"
            netmask             = "24"
            gateway             = "10.xx.xx.254"
            num_cpus            = "2"
            memory              = "4096"
            disk_size           = "30"
            user                = "admin"
            password            = "************"
            cmd                 = "/root/kubernetes/cluster_init.sh"
        }
        master-2 = {
            vsphere_datastore   = "Datastore_name"
            vsphere_vm_template = "tmpl-centos-7.7-k8s-1.2.5"
            ip_address          = "10.xx.xx.xx"
            netmask             = "24"
            gateway             = "10.xx.xx.254"
            num_cpus            = "2"
            memory              = "4096"
            disk_size           = "30"
            user                = "admin"
            password            = "************"
            cmd                 = "/root/kubernetes/cluster_join.sh"
        }
        master-3 = {
            vsphere_datastore   = "Datastore_name"
            vsphere_vm_template = "tmpl-centos-7.7-k8s-1.2.5"
            ip_address          = "10.xx.xx.xx"
            netmask             = "24"
            gateway             = "10.xx.xx.254"
            num_cpus            = "2"
            memory              = "4096"
            disk_size           = "30"
            user                = "admin"
            password            = "************"
            cmd                 = "/root/kubernetes/cluster_join.sh"
        }
    }

    workers = {
        ingress-controller-1 = {
            vsphere_datastore   = "Datastore_name"
            vsphere_vm_template = "tmpl-centos-7.7-k8s-1.2.5"
            ip_address          = "10.xx.xx.xx"
            netmask             = "24"
            gateway             = "10.xx.xx.254"
            num_cpus            = "1"
            memory              = "2048"
            disk_size           = "30"
            user                = "admin"
            password            = "************"
            role                = "ingress-controller"

        }
        ingress-controller-2 = {
            vsphere_datastore   = "Datastore_name"
            vsphere_vm_template = "tmpl-centos-7.7-k8s-1.2.5"
            ip_address          = "10.xx.xx.xx"
            netmask             = "24"
            gateway             = "10.xx.xx.254"
            num_cpus            = "1"
            memory              = "2048"
            disk_size           = "30"
            user                = "admin"
            password            = "************"
            role                = "ingress-controller"
        }
        worker-1 = {
            vsphere_datastore   = "Datastore_name"
            vsphere_vm_template = "tmpl-centos-7.7-k8s-1.2.5"
            ip_address          = "10.xx.xx.xx"
            netmask             = "24"
            gateway             = "10.xx.xx.254"
            num_cpus            = "8"
            memory              = "16384"
            disk_size           = "60"
            user                = "admin"
            password            = "************"
            role                = "worker"
        }
        worker-2 = {
            vsphere_datastore   = "Datastore_name"
            vsphere_vm_template = "tmpl-centos-7.7-k8s-1.2.5"
            ip_address          = "10.xx.xx.xx"
            netmask             = "24"
            gateway             = "10.xx.xx.254"
            num_cpus            = "8"
            memory              = "16384"
            disk_size           = "60"
            user                = "admin"
            password            = "************"
            role                = "worker"
        }
        worker-3 = {
            vsphere_datastore   = "Datastore_name"
            vsphere_vm_template = "tmpl-centos-7.7-k8s-1.2.5"
            ip_address          = "10.xx.xx.xx"
            netmask             = "24"
            gateway             = "10.xx.xx.254"
            num_cpus            = "8"
            memory              = "16384"
            disk_size           = "60"
            user                = "admin"
            password            = "************"
            role                = "worker"
        }
        agent-jenkins-1 = {
            vsphere_datastore   = "Datastore_name"
            vsphere_vm_template = "tmpl-centos-7.7-k8s-1.2.5"
            ip_address          = "10.xx.xx.xx"
            netmask             = "24"
            gateway             = "10.xx.xx.254"
            num_cpus            = "8"
            memory              = "16384"
            disk_size           = "60"
            user                = "admin"
            password            = "************"
            role                = "worker"
        }
        agent-jenkins-2 = {
            vsphere_datastore   = "Datastore_name"
            vsphere_vm_template = "tmpl-centos-7.7-k8s-1.2.5"
            ip_address          = "10.xx.xx.xx"
            netmask             = "24"
            gateway             = "10.xx.xx.254"
            num_cpus            = "8"
            memory              = "16384"
            disk_size           = "60"
            user                = "admin"
            password            = "************"
            role                = "worker"
        }                
    }

    ha-proxy = {
        name                  = "k8s-api"
        vsphere_datastore     = "Datastore_name"
        vsphere_vm_template   = "tmpl-centos-7.7-k8s-haproxy-1.0.1"
        ip_address            = "10.xx.xx.xx"
        netmask               = "24"
        gateway               = "10.xx.xx.254"
        num_cpus              = "1"
        memory                = "2048"
        disk_size             = "30"
        user                  = "admin"
        password              = "************"
        cmd                   = "systemctl restart haproxy18.service"        
    }

    ha-proxy-ing = {
        name                  = "k8s-ingress"
        vsphere_datastore     = "Datastore_name"
        vsphere_vm_template   = "tmpl-centos-7.7-k8s-haproxy-1.0.1"
        ip_address            = "10.xx.xx.xx"
        netmask               = "24"
        gateway               = "10.xx.xx.254"
        num_cpus              = "1"
        memory                = "2048"
        disk_size             = "30"
        user                  = "admsrv"
        password              = "sud04win"
        cmd                   = "systemctl restart haproxy18.service"
    }
}
```

### Extrait de rôles Ansible

**Ansible** a été utilisé à plusieurs reprises avec **Packer** ou **Terraform**, soit pour provisionner des fichiers sur les machines, soit pour modifier la configuration des OS, démarrer des services avec *systemD* ou encore installer des paquets *YUM*.

```yaml
- name: Update yum repositories
  yum:
    name: '*'
    state: latest

- name: Pull Docker images for K8S offline install
  get_url:
    url: "http://repoyum.local/docker-images/kubernetes/{{ item }}"
    dest: /tmp
  with_items:
    - coredns.tar
    - etcd.tar
    - kube-apiserver.tar
    - kube-controller-manager.tar
    - kube-proxy.tar
    - kube-scheduler.tar
    - pause.tar
    - calico_cni_3.11.3.tar
    - calico_kube_controllers_3.11.3.tar
    - calico_node_3.11.3.tar
    - calico_pod2daemon_flexvol_3.11.3.tar

- name: Setup Docker daemon
  copy:
    src: "files/daemon.json"
    dest: "/etc/docker/"
    mode: "0644"
    owner: "root"
    group: "root"

- name: Sysctl bridge conf file
  copy:
    src: "files/k8s.conf"
    dest: "/etc/sysctl.d/"
    mode: "0644"
    owner: "root"
    group: "root"

- name: Remove swap into /etc/fstab
  command: sed -i '/swap/d' /etc/fstab

- name: Disable swap with ansible module
  command: swapoff -a    
  
- name: Remove SWAP logical volume
  shell: 
    cmd: lvremove -fy /dev/mapper/VG0-swap
  
- name: Update /etc/default/grub
  copy:
    src: "files/grub"
    dest: "/etc/default/"
    mode: "0644"
    owner: "root"
    group: "root"
  
- name: Generate new grub2.cfg
  shell: 
    cmd: grub2-mkconfig > /etc/grub2.cfg

- name: Ensure SELinux is permanently disabled
  shell: 
    cmd: sed -i --follow-symlinks 's/^SELINUX=enforcing/SELINUX=disabled/' /etc/sysconfig/selinux

- name: Disable with ansible module
  selinux:
    state: disabled

- name: Copy setup scripts
  copy:
    src: "files/k8s_load_img.sh"
    dest: "/tmp/"
    mode: "0655"
    owner: "root"
    group: "root"

- name: Load Docker images from archives
  script: /tmp/k8s_load_img.sh

- name: Copy CALICO network plugin manifest
  copy:
    src: "files/calico.yaml"
    dest: "/tmp/"
    mode: "0655"
    owner: "root"
    group: "root"

- name: Copy cluster init script
  copy:
    src: "files/cluster_init.sh"
    dest: "/root/kubernetes/"
    mode: "0751"
    owner: "root"
    group: "root"

- name: Copy cluster join script
  copy:
    src: "files/cluster_join.sh"
    dest: "/root/kubernetes/"
    mode: "0751"
    owner: "root"
    group: "root"

- name: Copy worker init script
  copy:
    src: "files/worker_init.sh"
    dest: "/root/kubernetes/"
    mode: "0751"
    owner: "root"
    group: "root"

- name: Copy kubeadm-config.yaml script
  copy:
    src: "files/kubeadm-config.yaml"
    dest: "/root/kubernetes/"
    mode: "0644"
    owner: "root"
    group: "root"

- name: Copy id_rsa.pub
  copy:
    src: "files/id_rsa.pub"
    dest: "/root/.ssh/"
    mode: "0644"
    owner: "root"
    group: "root"

- name: Copy id_rsa
  copy:
    src: "files/id_rsa"
    dest: "/root/.ssh/"
    mode: "0600"
    owner: "root"
    group: "root"

- name: Copy authorized_keys
  copy:
    src: "files/authorized_keys"
    dest: "/root/.ssh/"
    mode: "0644"
    owner: "root"
    group: "root"

- name: Install kubeadm kubelet and kubectl packages (offline)
  yum:
    name: "{{ packages }}"
  vars:
    packages:
    - kubeadm
    - kubelet
    - kubectl
  notify: start kubelet
```

## Architecture du cluster client 🧱

Le cluster du client a été déployé avec `kubeadm` et en respecant les bonnes practiques de la [documentation Kubernetes](https://kubernetes.io/docs/setup/production-environment/tools/kubeadm/setup-ha-etcd-with-kubeadm/) :

![cluster_k8s](cluster_dtg.png)

Tout le travail d'*IaC* effectué en amont avec la stack | **Packer** | **Terrraform** | **Ansible** | nous a permis d'avoir un cluster qui se déploie et s'initialise en quelques minutes seulement.  
Bien entendu, pour que tout cela fonctionne, nous avons été obligés d'intégrer plusieurs éléments importants dans nos templates de VMs **Packer** :
- l'installation via YUM des binaires `docker`, `kubelet` et `kubeadm` dans */usr/local/bin*
- les certificats SSL ainsi que les autorités de certification du client (*Root CA*) dans */etc/pki/tls/certs/*
- les scripts Bash et les clés SSH
- les différents fichiers de configuration (*kube-config, calico...*)

## Gestion des registry Docker 🐳 / Helm ⚙️

Le contexte client impliquait l'**abscence d'accès direct à Internet** sur l'ensemble de l'infrastrcuture.
Il a donc fallu récupérer toutes les images **Docker** nécessaires à la création du cluster **Kubernetes** et aux **Charts Helm** dans un espace GitLab prévu à cet effet.  
Cette fonctionnalité intégrée à GitLab est connue sous le nom de `shared-registry`. Elle s'active au sein d'un projet GitLab et est ensuite accessible sur le port `5050`.

**Utilisation de la `shared-registry` pour nos images Docker 🐳 :**

![registry_dockers](docker_registry.png)

**Docker login** :
```bash
docker login gitlab.client.local:5050
```

**Docker build / push :**
```bash
docker build -t gitlab.client.local:5050/pole_ie/docker/shared-registry .
docker push gitlab.client.local:5050/pole_ie/docker/shared-registry
```

**Le même procédé a été utilisé pour stocker nos Charts Helm ⚙️ :**

![registry_dockers](helm_registry.png)

**Helm registry login** :
```bash
helm registry login gitlab.client.local:5050
```

**Helm save / push :**
```bash
cd charts/<chart name>
helm chart save . gitlab.client.local:5050/pole_ie/helm-charts/<chart name>:<chart version>
helm chart push gitlab.client.local:5050/pole_ie/helm-charts/<chart name>:<chart version>
```

## Connexion aux clusters 🖥️
Une fois que le cluster s'initialise, on récupère les tokens d'admin sur un des noeuds master du control-plane, puis on le place sur notre poste de travail dans notre *home_directory* `.kube/`.

{{% notice warning "Warning" %}}
Attention, cette manière de faire n'est pas une bonne pratique.  
Il faut mettre en place une solution d'authentification pour les utilisateurs.
{{% /notice %}}

L'idéal est d'avoir une solution d'authentification sur l'annuaire de l'entreprise (*OIDC, SAML...etc*).

Dans notre contexte, chaque fichier correspond à un cluster **Kubernetes** (*test, dev, prod*) :

```bash
❯ ll .kube/
drwxr-xr-x     - root 24 Aug 17:35 -N  backups/
.rw------- 5.4Ki root 14 Oct 12:03 -N  config
.rw------- 5.4Ki root 20 Dec 16:54 -N  development
.rw------- 7.5Ki root 20 Dec 16:54 -N  production
.rw------- 5.5Ki root 24 Aug 17:38 -N  sandbox
```
Cette configuration permet de *"switcher"* rapidement d'un cluster à un autre par l'intermédiaire du binaire `kubectl` :

```bash
❯ grep -ri "KUBECONFIG" ~/.zshrc
export KUBECONFIG=$KUBECONFIG:$HOME/.kube/production:$HOME/.kube/development:$HOME/.kube/sandbox
```

Avec l'aide d'alias, on peut alors lister ou utiliser les différents contextes disponibles (donc changer de cluster)

```bash
❯ kcgc
CURRENT   NAME                          CLUSTER       AUTHINFO           NAMESPACE
*         dev                           development   kubernetes-admin   argocd
          kubernetes-admin@kubernetes   kubernetes    kubernetes-admin   monitoring
          prod                          production    production-admin   argocd
❯ kcuc prod
Switched to contexte "prod".
❯ kcgc
CURRENT   NAME                          CLUSTER       AUTHINFO           NAMESPACE
          dev                           development   kubernetes-admin   argocd
          kubernetes-admin@kubernetes   kubernetes    kubernetes-admin   monitoring
*         prod                          production    production-admin   argocd
```

## Déployer avec Argo CD ! 🐙

Au début du projet, les applications étaient déployées dans **Kubernetes** via des `Charts Helm` depuis nos postes de travail respectifs, à l'aide du binaire [Helm](https://helm.sh/docs/intro/install/) dans sa **version 3**.

La tâche était assez laborieuse car il fallait d'abord configurer nos postes de travail, se souvenir du nom de la **Chart**, dans quel repo *Git* elle se trouve, se synchroniser sur la bonne branche...  
De plus, nous n'avions pas directement la possibilité de connaître le dernier état dans lequel s'était déployée la *Chart* **Helm**.

**Exemple de déploiement d'une Chart Helm à la main** :

Configurer la variable `HELM_EXPERIMENTAL_OCI`
```bash
export HELM_EXPERIMENTAL_OCI=1
```

Se connecter en CLI à la registry GitLab qui contient nos Charts Helm :
```bash
helm registry login gitlab.client.local:5050
```

Ajout du repo Helm :
```bash
helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
```

Premier déploiement :
```bash
# Met à jour les dépendances
helm dep up
# Déploie la chart
helm install --namespace <namespace> <chart-name> . -f values.yaml -f values.<env>.yaml -f secrets.<env>.yaml
```

Mise à jour de la Chart :
```bash
helm upgrade -i --namespace ingress-controller ingress-nginx . -f values.yaml
```

Voir les différences entre deux mises à jour de Charts :
```bash
helm diff upgrade --namespace ingress-controller ingress-nginx . -f values.yaml
```

Heureusement, un nouvel outil est apparu dans le catalogue de la **CNCF** et vient largement simplifier les déploiements dans **Kubernetes**.

Il se fait connaitre sous le nom de [Argo CD](https://argoproj.github.io/cd/) !

Il simplifie tout ce qui a été mentionné ci-dessus et permet de se concentrer uniquement sur les processus de déploiements.  
**Argo CD** ne remplace en aucun cas des outils comme **Jenkins**, mais propose via une interface web intuitive, une vue en temps réel sur l’état de santé des applications déployées.  
Il détecte ce qui a changé par rapport aux derniers commits et exécute un flux de synchronisation qui déploie automatiquement les changements sur le(s) cluster(s) **Kubernetes**.

![argocd](argocd.png)

Il a le gros avantage d'avoir été pensé "*As Code*", c'est à dire qu'on peut le configurer directement via une **Chart** `Helm` de façon à ce qu'il soit *Up & Ready* dès qu'il est déployé dans le cluster.  
De plus, on peut très bien disposer d'une instance `Argo CD` *Master* qui déploie ensuite une autre instance `Argo CD` !

> What ??? Mais pourquoi faire ?! 🤔

Cette méthode permet de couvrir plusieurs cas d'usage :
- un nouveau cluster est déployé sur l'infra, on peut alors *"piloter"* les déploiements depuis l'instance `Argo CD` *Master*
- bénéficier d'instances auto-gérées

Sa principale force réside dans le fait de pouvoir se sourcer directement sur des repos *Git* pour détecter immédiatement les changements d'états des projets déployés.

![argocd](argocd_diagram.png)

Le changement d'état est visuel et la validation humaine se fait d'un simple *clic* sur le bouton "**Sync**".  
Avant de valider, il est bien sûr possible d'afficher les changements sous la forme d'un `git-diff`, qui revient à faire un `helm diff upgrade` lorsqu'on déploie à la main.

![argocd](argocd_app_diff.png)

## Pipelines CI/CD 🔂

La méthodologie *DevOps* implique que des règles communes entre *Dev* et *Ops* soient définies en avance de phase de manière à ce que tout le monde soit raccord sur la façon de travailler.

Dans ce contexte, nous avons défini des règles de déploiement qui s'articulent de la manière suivante :
{{% notice warning "Warning" %}}
Il est indispensable de créer un fichier de values pour chaque environnement à déployer, à la racine de la chart Helm
{{% /notice %}}


    values.yaml (contient les valeurs communes à tous les environnements)
    values.dev.yaml
    values.integration.yaml
    values.recette.yaml
    values.preproduction.yaml
    values.production.yaml

Toute autre fichier de values qui ne respecte pas cette nomenclature fera échouer le job ou ne sera pas pris en compte.

Chaque équipe de développement est autorisée à déployer des namespaces avec la convention de nommage suivante :

**En developpement** :

    project-dev
    project-integration
    project-recette
    project-preproduction

**En production** :

    project

Les jobs de déploiement sont déclarés dans un noeud **Jenkins** à l'intérieur d'une `multi-branch pipeline` en **Groovy**.

Le serveur **Jenkins** principal fait appel à un agent **Jenkins** présent dans un *namespace* **Kubernetes**.
Cet agent se connecte à un repo GitLab interne sur la bonne branche, puis exécute les différents steps **Helm** comme si on déployait une *Chart* manuellement :
```bash
helm registry login
helm dependency update
helm diff upgrade
helm upgrade -i
```


```groovy
String environment = ENV
String namespace = "${PROJECT}-${environment == 'development' ? 'dev' : environment}"
boolean humanValidation = Boolean.parseBoolean(HUMAN_VALIDATION)

podTemplate {
  node('jenkins-agent-pole-ie') {
        stage('Checkout repository'){
            checkout([
                $class: 'GitSCM',
                doGenerateSubmoduleConfigurations: false,
                extensions: [],
                branches: [
                    [name: BRANCH],    
                ],
                userRemoteConfigs: [[credentialsId: 'jenkins-gitlab-registry-dev', url: REPOSITORY_URL]]
            ])
        }
        container('helm') {
            stage("Helm dependency update") {
                withCredentials([
                    usernamePassword(credentialsId: 'jenkins-gitlab-registry-dev',
                    usernameVariable: 'username',
                    passwordVariable: 'password')
                ]) {
                    sh "helm registry login gitlab.client.local:5050 -u '${username}' -p '${password}'"
                }
                sh "cd ${CHART_PATH}; helm dependency update"
            }
            stage("Generate Helm Diff"){
                sh "cd ${CHART_PATH}; helm diff upgrade --allow-unreleased --namespace ${namespace} ${PROJECT} . -f values.yaml -f values.${environment}.yaml"
            }

            stage("Validate Deployment - Waiting for Human validation") {
                if (humanValidation) {
                    input(message: "Check Helm diff. Do you want to proceed to the deployment ?")
                }
            }
      
            stage("Deploy the application"){
                sh "cd ${CHART_PATH}; helm upgrade -i --namespace ${namespace} ${PROJECT} . -f values.yaml -f values.${environment}.yaml"
            }
        }
    }
}
```


## Et comment on monitore ? 🚥

Maintenant qu'on a vu comment était déployées les applications au sein du cluster, intéressons nous à l'observabilité de celles-ci.  
C'est une brique importante, surtout lorsque les premières applications arrivent en production.  
Le *monitoring* dans **Kubernetes** peut être déployé rapidement, mais il faudra y consacrer **beaucoup** de temps pour obtenir quelque chose de robuste et fiable.

Tout le monde connait des outils de monitoring basés sur le protocole `SNMP`, tels que [Centreon] ou [Nagios] et il est d'ailleurs possible de monitorer un cluster **Kubernetes** via des plugins payants.

Il est cependant préférable d'utiliser des outils prévus à cet effet. **Kubernetes** expose un nombre incalculable de métriques à travers son `api-server`.  
Ces métriques peuvent donc facilement être envoyées dans un *exporter* [Prometheus](https://prometheus.io/) et être affichées à travers des graphiques proposés par [Grafana](https://grafana.com/grafana/).

Un projet **GitHub** nommé [kube-prometheus-stack](https://github.com/prometheus-community/helm-charts/blob/main/charts/kube-prometheus-stack/README.md) permet d'avoir toute la stack "Monitoring" dans un cluster **Kubernetes**.  
Elle se compose de plusieurs éléments :
- [Prometheus-operator]()
- [Grafana]()
- [Alertmanager]()

[Prometheus]() donne la possibilité de personnaliser des règles à partir des métriques qu’il récupère via l’API **Kubernetes**.  
Par exemple, si on veut vérifier l’état des **Ingress** dans un namespace précis, c’est possible. Il faut connaître le nom de la métrique et jouer ensuite avec les opérateurs :

![argocd](prometheus.png)

[Grafana]() permet de gérer l'affichage des données qui lui sont transmises via une `datasource` (*Prometheus, InfluxDB...*).  
Dans la *Chart* `kube-prometheus-stack`, tous les dashboards utiles à **Kubernetes** sont déjà présents.  
Il suffit d'attendre qu'un certain nombre de données soit collecté pour que les dashboards soient alimentés.

![argocd](grafana.png)

Quant à [Alertmanager](), il intercepte les dépassements de seuils pour les transformer en alertes puis envoie les notifications via des **receivers** (*mails, webhooks Slack, Teams...etc*).

**Extrait de notifications Alertmanager par e-mails** :

```yaml
  additionalPrometheusRulesMap:
   rule-name:
    groups:
      #=========================
      # ALERTING RULES !
      #=========================
      ## Ref: https://awesome-prometheus-alerts.grep.to/rules.html
      - name: base_project
        rules:
          - alert: HttpsCheck200
            expr: probe_http_status_code {namespace!~".*-(dev|integration|preprod|recette)"} != 200
            for: 3m
            labels:
              severity: critical
              project: base_project
            annotations:
              description: 'HTTPS probe en échec sur {{ $labels.instance }}!'
              summary: '${{ $labels.instance }} est indisponible (status code != {{ $value }}) !'
      - name: app_name
        rules:
          - alert: PodRestarting
            expr: delta(kube_pod_container_status_restarts_total{namespace="app_name-.*"}[4m]) > 1
            for: 1m
            labels:
              severity: warning
              project: app_name
            annotations:
              description: 'Le pod {{ $labels.pod }} a été redémarré !'
              summary: 'Un restart de pod a été détecté !'        

          - alert: PodCrashLoopBackOff
            expr: delta(kube_pod_container_status_restarts_total{namespace="app_name-.*"}[8m]) > 5
            for: 1m
            labels:
              severity: critical
              project: app_name
            annotations:
              description: 'Le pod {{ $labels.pod }} a redémarré plusieurs fois !'
              summary: 'Un pod a restart plus de 5 fois et est probablement en CrashLoopBackOff !'
          
          - alert: HttpsCheck200
            expr: probe_http_status_code {namespace=~"app_name-.*"} != 200
            for: 1m
            labels:
              severity: critical
              project: app_name
            annotations:
              description: 'HTTPS probe en échec sur {{ $labels.instance }}!'
              summary: '${{ $labels.instance }} est indisponible (status code == {{ $value }}) !'

  alertmanager:
    ## Alertmanager configuration directives
    ## ref: https://prometheus.io/docs/alerting/configuration/#configuration-file
    config:
      global:
        smtp_from: 'alerting@client.local'   

      receivers:
      - name: 'admin'
        email_configs:
        - send_resolved: true
          to: exploit-srv@client.local
          from: alerting@client.local
          smarthost: mailhost.client.local:25
          headers:
            From: alerting@client.local
            Subject: '[alertmanager] - Alertes monitoring !'
            To: exploit-srv@client.local
          require_tls: false

      - name: 'app_name'
        email_configs:
        - send_resolved: true
          to: dev-team@client.local
          from: alerting@client.local
          smarthost: mailhost.client.local:25
          headers:
            From: alerting@client.local
            Subject: '[k8s-cluster-dev] - Alerte(s) dans l'application $name !'
            To: dev-team@client.local
          require_tls: false          

    ingress:
      hosts:
        - alertmanager.dev.client.local
      tls: 
      - secretName: kube-prometheus-stack-secrets-default-wildcard
        hosts:
        - alertmanager.dev.client.local
      paths:
      - /
      pathType: Prefix              
```

Le template de mail **Alertmanager** réceptionné ressemble à ça :

![argocd](alertmanager.png)

Alertmanager intègre aussi des règles d'alerting déjà prévues pour **Kubernetes** mais il faut avoir en tête qu'un alerting efficace est un alerting qui couvre les principaux types de pannes sur une application en production.
- restart de pods
- pods en CrashLoopBackOff
- healthcheck sur les URLs exposées par les ingress

## Conclusion

Le démarrage du projet a été compliqué car l’équipe des Ops partait de zéro et n’était pas formée aux
méthodes de travail induites par tous ces nouveaux outils.

- La courbe d’apprentissage liée à `Kubernetes` | `Helm` est assez longue et complexe.
- Un renforcement de l’équipe et une montée en compétences a été nécessaire.
- La stratégie de l’équipe a été repensée pour mieux répartir la charge de travail entre *Build & Run* et la transformation *DevOps*

En dépit de ces difficultés et grâce à l'implication de chacun, plusieurs applications métier ont déjà été migrées dans les clusters Kubernetes hors-production du client.
 
Une des étapes importante du projet à été la mise en production de la première application du client au mois de Juillet 2021.  
Depuis, les déploiements en Prodution se sont poursuivis et l'équipe dispose d'une roadmap conséquente pour venir améliorer la robustesse des clusters.